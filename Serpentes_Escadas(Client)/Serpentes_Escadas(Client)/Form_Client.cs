﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Media;
using WMPLib;

namespace Serpentes_Escadas_Client_
{

    public partial class Form_Client : Form
    {

        #region Completo

        #region Variaveis

        Jogador MC1 = new Jogador();
        Jogador MC2 = new Jogador();

        List<Mapa> ListaCoordenadasMC1 = new List<Mapa>();
        List<Mapa> ListaCoordenadasMC2 = new List<Mapa>();

        List<Jogador> ListaJogadores = new List<Jogador>();
        
        TcpClient TcpClient = new TcpClient();//liga,recebe e envia dados do cliente

        byte[] Dados;//Recebe dados do cliente;

        private int VezJogador;//1-P1,2-P2,3-P3,4-P4
        private int FaceDado = 0;
        private bool Vitorioso = false;

        #endregion

        public Form_Client()
        {

            InitializeComponent();

            jogador1.Parent = PictureBox_Board;
            jogador2.Parent = PictureBox_Board;
            
            VezJogador = 1;




        }

        private void Form1_Load(object sender, EventArgs e)
        {

            ListaCoordenadasMC1 = Mapa.CriarMapas();

            foreach (var x in ListaCoordenadasMC1)
            {
                x.X -= 12;
                x.Y -= 12;
            }

            foreach (var x in ListaCoordenadasMC1)
            {

                ListaCoordenadasMC2.Add(new Mapa(x.X + 42, x.Y, x.NmrQuadrado));

            }

            MC1.Coordenadas = (from coord in ListaCoordenadasMC1 where coord.NmrQuadrado == 1 select coord).Single();
            MC2.Coordenadas = (from coord in ListaCoordenadasMC2 where coord.NmrQuadrado == 1 select coord).Single();

            jogador1.Location = new Point(4, 781);
            jogador2.Location = new Point(46, 781);

        }

        /// <summary>
        /// Seleciona a cor do peão.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBox_Cor_SelectedIndexChanged(object sender, EventArgs e)
        {

            switch (ComboBox_Cor.Text)
            {
                case "Vermelho":
                    PictureBox_P1.ImageLocation = @"Imagens\Red_Piece.png";
                    Label_NomeP2.ForeColor = Color.DarkRed;
                    break;
                case "Azul":
                    PictureBox_P1.ImageLocation = @"Imagens\Blue_Piece.png";
                    Label_NomeP2.ForeColor = Color.Blue;
                    break;
                case "Amarelo":
                    PictureBox_P1.ImageLocation = @"Imagens\Yellow_Piece.png";
                    Label_NomeP2.ForeColor = Color.FromArgb(225,225,0);
                    break;
                case "Preto":
                    PictureBox_P1.ImageLocation = @"Imagens\Black_Piece.png";
                    Label_NomeP2.ForeColor = Color.DarkGray;
                    break;
                case "Rosa":
                    PictureBox_P1.ImageLocation = @"Imagens\Pink_Piece.png";
                    Label_NomeP2.ForeColor = Color.Pink;
                    break;
                case "Verde":
                    PictureBox_P1.ImageLocation = @"Imagens\Green_Piece.png";
                    Label_NomeP2.ForeColor = Color.DarkGreen;
                    break;
            }

        }

        /// <summary>
        /// Gera o número de casas que o jogador irá avançar.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Dice_Click(object sender, EventArgs e)
        {

            if (VezJogador == 2)
            {

                #region Lançamento do dado

                Random r = new Random();

                int aux = r.Next(1, 7);         

                switch (aux)
                {
                    case 1:
                        PictureBox_Dado.ImageLocation = @"Imagens\Dice1.png";
                        break;
                    case 2:
                        PictureBox_Dado.ImageLocation = @"Imagens\Dice2.png";
                        break;
                    case 3:
                        PictureBox_Dado.ImageLocation = @"Imagens\Dice3.png";
                        break;
                    case 4:
                        PictureBox_Dado.ImageLocation = @"Imagens\Dice4.png";
                        break;
                    case 5:
                        PictureBox_Dado.ImageLocation = @"Imagens\Dice5.png";
                        break;
                    case 6:
                        PictureBox_Dado.ImageLocation = @"Imagens\Dice6.png";
                        break;
                }

                Mover(aux, MC2);


                Vitoria();

                #endregion

                VezJogador = 1;

                FaceDado = aux;

                SendData();

            }
            else
            {

                MessageBox.Show("Não é a sua vez!");
                return;

            }

        }

        /// <summary>
        /// Move a pictureBox.
        /// </summary>
        /// <param name="numero"></param>
        private void Mover(int numero, Jogador mc)
        {

            List<Mapa> x = new List<Mapa>();

            if (mc == MC1)
            {

                x = ListaCoordenadasMC1;

            }
            else if (mc == MC2)
            {

                x = ListaCoordenadasMC2;

            }            

            if (numero + mc.Coordenadas.NmrQuadrado > 100)
            {

                Superior100(numero, mc);

            }
            else
            {

                mc.Coordenadas = (from cd in x where mc.Coordenadas.NmrQuadrado + numero == cd.NmrQuadrado select cd).Single();

            }

            if (mc == MC1)
            {
                jogador1.Invoke(new Action<Point>(InvocarP1), new Point(mc.Coordenadas.X, mc.Coordenadas.Y));
                System.Threading.Thread.Sleep(100);


                Escada_Snake(mc);

                jogador1.Invoke(new Action<Point>(InvocarP1), new Point(mc.Coordenadas.X, mc.Coordenadas.Y));

            }
            else if (mc == MC2)
            {
                jogador2.Invoke(new Action<Point>(InvocarP2), new Point(mc.Coordenadas.X, mc.Coordenadas.Y));
                System.Threading.Thread.Sleep(100);


                Escada_Snake(mc);

                jogador2.Invoke(new Action<Point>(InvocarP2), new Point(mc.Coordenadas.X, mc.Coordenadas.Y));

            }            

        }

        /// <summary>
        /// Verifica se o valor ultrapassa a casa 100.
        /// </summary>
        private void Superior100(int nmr, Jogador mc)
        {

            List<Mapa> x = new List<Mapa>();

            if (mc == MC1)
            {
                x = ListaCoordenadasMC1;
            }
            else if (mc == MC2)
            {
                x = ListaCoordenadasMC2;
            }


            if (nmr + mc.Coordenadas.NmrQuadrado > 100)
            {
                mc.Coordenadas = (from cd in x where (100 - (nmr + mc.Coordenadas.NmrQuadrado - 100)) == cd.NmrQuadrado select cd).Single();
            }

        }

        /// <summary>
        /// Verifica a localização das escadas e cobras.
        /// </summary>
        private void Escada_Snake(Jogador player)
        {
            List<Mapa> x = new List<Mapa>();

            if (player == MC1)
            {
                x = ListaCoordenadasMC1;
            }
            else if (player == MC2)
            {
                x = ListaCoordenadasMC2;
            }

            switch (player.Coordenadas.NmrQuadrado)
            {
                case 2:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 38 select cd).Single();

                    break;
                case 7:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 14 select cd).Single();

                    break;
                case 8:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 31 select cd).Single();

                    break;
                case 15:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 26 select cd).Single();

                    break;
                case 16:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 6 select cd).Single();

                    break;
                case 21:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 42 select cd).Single();

                    break;
                case 28:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 84 select cd).Single();

                    break;
                case 36:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 44 select cd).Single();

                    break;
                case 46:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 25 select cd).Single();

                    break;
                case 49:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 11 select cd).Single();

                    break;
                case 51:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 67 select cd).Single();

                    break;
                case 62:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 19 select cd).Single();

                    break;
                case 64:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 60 select cd).Single();

                    break;
                case 71:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 91 select cd).Single();

                    break;
                case 74:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 53 select cd).Single();

                    break;
                case 78:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 98 select cd).Single();

                    break;
                case 87:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 94 select cd).Single();

                    break;
                case 89:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 68 select cd).Single();

                    break;
                case 92:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 88 select cd).Single();

                    break;
                case 95:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 75 select cd).Single();

                    break;
                case 99:

                    player.Coordenadas = (from cd in x where cd.NmrQuadrado == 80 select cd).Single();

                    break;
            }
        }

        /// <summary>
        /// Verifica se o jogador chega à casa 100.
        /// </summary>
        private void Vitoria()
        {

            if (Vitorioso)
            {

                MessageBox.Show(MC1.Nome + " ganhou o jogo!\nComeçando de novo.");
                Vitorioso = false;
                goto Vitoria;
            }

            if (MC1.Coordenadas.NmrQuadrado == 100)
            {
                MessageBox.Show(MC1.Nome + " ganhou o jogo!\nComeçando de novo.");
            }
            else if (MC2.Coordenadas.NmrQuadrado == 100)
            {
                MessageBox.Show(MC2.Nome + " ganhou o jogo!\nComeçando de novo.");
            }
            else
            {
                return;
            }

            Vitorioso = true;

            Vitoria:

            MC1.Coordenadas = (from coord in ListaCoordenadasMC1 where coord.NmrQuadrado == 1 select coord).Single();
            MC2.Coordenadas = (from coord in ListaCoordenadasMC2 where coord.NmrQuadrado == 1 select coord).Single();

            jogador1.Invoke(new Action<Point>(InvocarP1), new Point(4, 781));
            jogador2.Invoke(new Action<Point>(InvocarP2), new Point(46, 781));

            Vitorioso = false;
            VezJogador = 1;

        }

        private void Button_Begin_Click(object sender, EventArgs e)
        {

            panel2.Visible = false;
            panel1.Visible = true;

            Button_Begin.Visible = false;
            Button_Connect.Visible = false;

        }

        /// <summary>
        /// Mete o servidor à escuta.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Connect_click(object sender, EventArgs e)
        {

            voltar:

            #region Validações

            if (string.IsNullOrWhiteSpace(TextBox_Nome.Text))
            {

                MessageBox.Show("Não inseriu um nome!", "Erro");
                TextBox_Nome.Select();
                return;
            }

            if (ComboBox_Cor.Text != "Vermelho" && ComboBox_Cor.Text != "Amarelo" && ComboBox_Cor.Text != "Azul" && ComboBox_Cor.Text != "Verde" && ComboBox_Cor.Text != "Rosa" && ComboBox_Cor.Text != "Preto")
            {

                MessageBox.Show("Não escolheu uma cor válida!", "Erro");
                ComboBox_Cor.Select();
                return;

            }

            #endregion

            Button_Begin.Visible = true;

            #region Dados do Jogador a enviar para o cliente

            MC2.Nome = TextBox_Nome.Text;

            MC2.ImagemJogador = PictureBox_P1.ImageLocation;

            Label_NomeP2.Text = TextBox_Nome.Text;

            PictureBox_P2.Image = PictureBox_P1.Image;

            jogador2.BackgroundImage = PictureBox_P1.Image;

            jogador2.BackgroundImageLayout = ImageLayout.Stretch;

            #endregion

            MessageBox.Show("Ligado ao servidor no IP " + TextBox_IP.Text + " no porto " + TextBox_Porto.Text + ".");

            try
            {

                TcpClient.BeginConnect(IPAddress.Parse(TextBox_IP.Text), int.Parse(TextBox_Porto.Text), onCompleteConnect, TcpClient);

            }
            catch
            {

                goto voltar;

            }

            Button_Connect.Visible = false;

            SendData();

        }

        /// <summary>
        /// Quando o Processo de connexão acaba.
        /// </summary>
        /// <param name="ar"></param>
        private void onCompleteConnect(IAsyncResult ar)
        {

            TcpClient cl;

            cl = ar.AsyncState as TcpClient;

            cl.EndConnect(ar);

            Dados = new byte[1460];

            TcpClient.GetStream().BeginRead(Dados, 0, Dados.Length, onCompleteRead, TcpClient);


        }

        /// <summary>
        /// Obtem os primeiros dados iniciais.(Nome, imagem, coordenadas, vez do jogador e numero de face).
        /// </summary>
        /// <param name="ar"></param>
        private void onCompleteRead(IAsyncResult ar)
        {

            TcpClient tcpc;

            string received = null;//Recebe uma string que se contem nome, imagem e coordenada, vez do jogador, nmr de dado e vitoria separadas por ';'

            string[] dadosarray = new string[6];//Separa os dados da string.

            tcpc = ar.AsyncState as TcpClient;

            int ReadBytes = 0;//Guarda a quantidade de bytes a ler do pacote.

            ReadBytes = tcpc.GetStream().EndRead(ar);

            received = Encoding.UTF8.GetString(Dados, 0, ReadBytes);

            dadosarray = received.Split(';');

            #region Dados do P1

            MC1.Nome = dadosarray[0];//Nome do jogador 1

            MC1.ImagemJogador = dadosarray[1];//Imagem do Jogador 1

            MC1.Coordenadas = (from cd in ListaCoordenadasMC1
                               where cd.NmrQuadrado == Convert.ToInt32(dadosarray[2])
                               select cd).Single();//Coordenadas do jogador 1

            VezJogador = Convert.ToInt32(dadosarray[3]);//A vez do jogador a seguir

            FaceDado = Convert.ToInt32(dadosarray[4]);//Nmr do dado recebido, para mover o jogador 1

            jogador1.ImageLocation = MC1.ImagemJogador;
            jogador1.Invoke(new Action<Point>(InvocarP1), new Point((from cd in ListaCoordenadasMC1
                                                                     where cd.NmrQuadrado == Convert.ToInt32(dadosarray[2])
                                                                     select cd.X).Single(),
                       (from cd in ListaCoordenadasMC1 where cd.NmrQuadrado == Convert.ToInt32(dadosarray[2]) select cd.Y).Single()));

            pictureBox_mc1.ImageLocation = MC1.ImagemJogador;
            Label_NomeP1.Invoke(new Action<string>(InvocarNomeMC1), MC1.Nome);

            Vitorioso = Convert.ToBoolean(dadosarray[5]);

            #endregion

            if (Vitorioso)
            {
                Vitoria();
            }

            Dados = new byte[1460];

            TcpClient.GetStream().BeginRead(Dados, 0, Dados.Length, onCompleteRead, TcpClient);

            panel1.Invoke(new Action<bool>(InvocarPainel), true);
        }

        /// <summary>
        /// Envia os dados para o cliente.
        /// </summary>
        private void SendData()
        {

            byte[] sendtxt = new byte[1460];

            if (TcpClient == null)
            {

                MessageBox.Show("Não tem clientes ativos.");
                return;

            }

            if (!TcpClient.Client.Connected)
            {

                MessageBox.Show("O cliente desligou-se.");
                return;

            }

            string jogador = MC2.Nome + ";" + MC2.ImagemJogador + ";" + MC2.Coordenadas.NmrQuadrado + ";" + VezJogador + ";" + FaceDado + ";" + Vitorioso;
            sendtxt = Encoding.UTF8.GetBytes(jogador);

            TcpClient.GetStream().BeginWrite(sendtxt, 0, sendtxt.Length, onCompleteWrite, TcpClient);

        }

        /// <summary>
        /// Completa o processo de envio.
        /// </summary>
        /// <param name="ar"></param>
        private void onCompleteWrite(IAsyncResult ar)
        {

            TcpClient tcpc = ar.AsyncState as TcpClient;

            tcpc.GetStream().EndWrite(ar);

        }

        #region Invocações

        /// <summary>
        /// Invoca a mudança da PictureBox do Jogador 1.
        /// </summary>
        /// <param name="obj"></param>
        private void InvocarP1(Point obj)
        {
            jogador1.Location = obj;
        }

        /// <summary>
        /// Invoca a mudança da PictureBox do Jogador 2.
        /// </summary>
        /// <param name="obj"></param>
        private void InvocarP2(Point obj)
        {
            jogador2.Location = obj;
        }

        /// <summary>
        /// Invoca a mudança da Visibilidade do painel 1.
        /// </summary>
        /// <param name="obj"></param>
        private void InvocarPainel(bool obj)
        {
            panel1.Enabled = obj;
        }

        /// <summary>
        /// Invoca a mudança de nome da label.
        /// </summary>
        /// <param name="obj"></param>
        private void InvocarNomeMC1(string obj)
        {

            Label_NomeP1.Text = obj;

        }


        #endregion

        #endregion

        private void Button_Disconnect_Click(object sender, EventArgs e)
        {

            Application.Exit();

        }

    }

}
