﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Serpentes_Escadas_Client_
{
    public class Jogador
    {

        public string Nome { get; set; }

        public Mapa Coordenadas { get; set; }

        public string ImagemJogador { get; set; }

    }
}
