﻿namespace Serpentes_Escadas_Server_
{
    partial class Form_Server
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Server));
            this.Button_Connect = new System.Windows.Forms.Button();
            this.TextBox_IP = new System.Windows.Forms.TextBox();
            this.TextBox_Porto = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PictureBox_Dado = new System.Windows.Forms.PictureBox();
            this.Label_NomeP1 = new System.Windows.Forms.Label();
            this.pictureBox_mc1 = new System.Windows.Forms.PictureBox();
            this.PictureBox_P2 = new System.Windows.Forms.PictureBox();
            this.Button_Dice = new System.Windows.Forms.Button();
            this.Label_DiceNumber = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Label_NomeP2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ComboBox_Cor = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.PictureBox_P1 = new System.Windows.Forms.PictureBox();
            this.TextBox_Nome = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Button_Begin = new System.Windows.Forms.Button();
            this.jogador2 = new System.Windows.Forms.PictureBox();
            this.jogador1 = new System.Windows.Forms.PictureBox();
            this.PictureBox_Board = new System.Windows.Forms.PictureBox();
            this.Button_Close = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Dado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_mc1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_P2)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_P1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jogador2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jogador1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Board)).BeginInit();
            this.SuspendLayout();
            // 
            // Button_Connect
            // 
            this.Button_Connect.Location = new System.Drawing.Point(929, 770);
            this.Button_Connect.Name = "Button_Connect";
            this.Button_Connect.Size = new System.Drawing.Size(99, 41);
            this.Button_Connect.TabIndex = 23;
            this.Button_Connect.Text = "Ligar";
            this.Button_Connect.UseVisualStyleBackColor = true;
            this.Button_Connect.Click += new System.EventHandler(this.Button_Connect_click);
            // 
            // TextBox_IP
            // 
            this.TextBox_IP.Location = new System.Drawing.Point(929, 714);
            this.TextBox_IP.Name = "TextBox_IP";
            this.TextBox_IP.Size = new System.Drawing.Size(143, 20);
            this.TextBox_IP.TabIndex = 24;
            this.TextBox_IP.Text = "10.2.3.19";
            // 
            // TextBox_Porto
            // 
            this.TextBox_Porto.Location = new System.Drawing.Point(929, 740);
            this.TextBox_Porto.Name = "TextBox_Porto";
            this.TextBox_Porto.Size = new System.Drawing.Size(143, 20);
            this.TextBox_Porto.TabIndex = 25;
            this.TextBox_Porto.Text = "23000";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.PictureBox_Dado);
            this.panel1.Controls.Add(this.Label_NomeP1);
            this.panel1.Controls.Add(this.pictureBox_mc1);
            this.panel1.Controls.Add(this.PictureBox_P2);
            this.panel1.Controls.Add(this.Button_Dice);
            this.panel1.Controls.Add(this.Label_DiceNumber);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.Label_NomeP2);
            this.panel1.Location = new System.Drawing.Point(929, 214);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(143, 494);
            this.panel1.TabIndex = 34;
            this.panel1.Visible = false;
            // 
            // PictureBox_Dado
            // 
            this.PictureBox_Dado.Location = new System.Drawing.Point(6, 303);
            this.PictureBox_Dado.Name = "PictureBox_Dado";
            this.PictureBox_Dado.Size = new System.Drawing.Size(124, 124);
            this.PictureBox_Dado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_Dado.TabIndex = 48;
            this.PictureBox_Dado.TabStop = false;
            // 
            // Label_NomeP1
            // 
            this.Label_NomeP1.AutoSize = true;
            this.Label_NomeP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_NomeP1.Location = new System.Drawing.Point(7, 101);
            this.Label_NomeP1.Name = "Label_NomeP1";
            this.Label_NomeP1.Size = new System.Drawing.Size(67, 13);
            this.Label_NomeP1.TabIndex = 47;
            this.Label_NomeP1.Text = "Jogador 1:";
            // 
            // pictureBox_mc1
            // 
            this.pictureBox_mc1.Location = new System.Drawing.Point(10, 6);
            this.pictureBox_mc1.Name = "pictureBox_mc1";
            this.pictureBox_mc1.Size = new System.Drawing.Size(72, 92);
            this.pictureBox_mc1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox_mc1.TabIndex = 46;
            this.pictureBox_mc1.TabStop = false;
            // 
            // PictureBox_P2
            // 
            this.PictureBox_P2.Location = new System.Drawing.Point(10, 153);
            this.PictureBox_P2.Name = "PictureBox_P2";
            this.PictureBox_P2.Size = new System.Drawing.Size(72, 92);
            this.PictureBox_P2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_P2.TabIndex = 45;
            this.PictureBox_P2.TabStop = false;
            // 
            // Button_Dice
            // 
            this.Button_Dice.Location = new System.Drawing.Point(9, 464);
            this.Button_Dice.Name = "Button_Dice";
            this.Button_Dice.Size = new System.Drawing.Size(127, 23);
            this.Button_Dice.TabIndex = 44;
            this.Button_Dice.Text = "Lançar dado";
            this.Button_Dice.UseVisualStyleBackColor = true;
            this.Button_Dice.Click += new System.EventHandler(this.Button_Dice_Click);
            // 
            // Label_DiceNumber
            // 
            this.Label_DiceNumber.AutoSize = true;
            this.Label_DiceNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_DiceNumber.Location = new System.Drawing.Point(64, 464);
            this.Label_DiceNumber.Name = "Label_DiceNumber";
            this.Label_DiceNumber.Size = new System.Drawing.Size(0, 25);
            this.Label_DiceNumber.TabIndex = 43;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 430);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 42;
            this.label5.Text = "Saiu o nº:";
            // 
            // Label_NomeP2
            // 
            this.Label_NomeP2.AutoSize = true;
            this.Label_NomeP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_NomeP2.Location = new System.Drawing.Point(7, 259);
            this.Label_NomeP2.Name = "Label_NomeP2";
            this.Label_NomeP2.Size = new System.Drawing.Size(67, 13);
            this.Label_NomeP2.TabIndex = 41;
            this.Label_NomeP2.Text = "Jogador 2:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.ComboBox_Cor);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.PictureBox_P1);
            this.panel2.Controls.Add(this.TextBox_Nome);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(929, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(140, 185);
            this.panel2.TabIndex = 45;
            // 
            // ComboBox_Cor
            // 
            this.ComboBox_Cor.FormattingEnabled = true;
            this.ComboBox_Cor.Items.AddRange(new object[] {
            "Vermelho",
            "Azul",
            "Amarelo",
            "Preto",
            "Rosa",
            "Verde"});
            this.ComboBox_Cor.Location = new System.Drawing.Point(7, 63);
            this.ComboBox_Cor.Name = "ComboBox_Cor";
            this.ComboBox_Cor.Size = new System.Drawing.Size(130, 21);
            this.ComboBox_Cor.TabIndex = 38;
            this.ComboBox_Cor.Text = "Escolha uma cor";
            this.ComboBox_Cor.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Cor_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 13);
            this.label3.TabIndex = 37;
            this.label3.Text = "Escolha a sua cor:";
            // 
            // PictureBox_P1
            // 
            this.PictureBox_P1.Location = new System.Drawing.Point(27, 90);
            this.PictureBox_P1.Name = "PictureBox_P1";
            this.PictureBox_P1.Size = new System.Drawing.Size(72, 92);
            this.PictureBox_P1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_P1.TabIndex = 36;
            this.PictureBox_P1.TabStop = false;
            // 
            // TextBox_Nome
            // 
            this.TextBox_Nome.BackColor = System.Drawing.Color.White;
            this.TextBox_Nome.Location = new System.Drawing.Point(6, 24);
            this.TextBox_Nome.MaxLength = 25;
            this.TextBox_Nome.Name = "TextBox_Nome";
            this.TextBox_Nome.Size = new System.Drawing.Size(131, 20);
            this.TextBox_Nome.TabIndex = 35;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 13);
            this.label1.TabIndex = 34;
            this.label1.Text = "Jogador 1:";
            // 
            // Button_Begin
            // 
            this.Button_Begin.Location = new System.Drawing.Point(929, 817);
            this.Button_Begin.Name = "Button_Begin";
            this.Button_Begin.Size = new System.Drawing.Size(99, 41);
            this.Button_Begin.TabIndex = 46;
            this.Button_Begin.Text = "Começar";
            this.Button_Begin.UseVisualStyleBackColor = true;
            this.Button_Begin.Visible = false;
            this.Button_Begin.Click += new System.EventHandler(this.Button_Begin_Click);
            // 
            // jogador2
            // 
            this.jogador2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.jogador2.BackColor = System.Drawing.Color.Transparent;
            this.jogador2.Location = new System.Drawing.Point(58, 793);
            this.jogador2.Name = "jogador2";
            this.jogador2.Size = new System.Drawing.Size(36, 0);
            this.jogador2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.jogador2.TabIndex = 42;
            this.jogador2.TabStop = false;
            // 
            // jogador1
            // 
            this.jogador1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.jogador1.BackColor = System.Drawing.Color.Transparent;
            this.jogador1.Location = new System.Drawing.Point(16, 793);
            this.jogador1.Name = "jogador1";
            this.jogador1.Size = new System.Drawing.Size(36, 0);
            this.jogador1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.jogador1.TabIndex = 41;
            this.jogador1.TabStop = false;
            // 
            // PictureBox_Board
            // 
            this.PictureBox_Board.Image = ((System.Drawing.Image)(resources.GetObject("PictureBox_Board.Image")));
            this.PictureBox_Board.Location = new System.Drawing.Point(12, 12);
            this.PictureBox_Board.Name = "PictureBox_Board";
            this.PictureBox_Board.Size = new System.Drawing.Size(905, 840);
            this.PictureBox_Board.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox_Board.TabIndex = 0;
            this.PictureBox_Board.TabStop = false;
            // 
            // Button_Close
            // 
            this.Button_Close.Image = ((System.Drawing.Image)(resources.GetObject("Button_Close.Image")));
            this.Button_Close.Location = new System.Drawing.Point(1040, 793);
            this.Button_Close.Name = "Button_Close";
            this.Button_Close.Size = new System.Drawing.Size(32, 32);
            this.Button_Close.TabIndex = 47;
            this.Button_Close.UseVisualStyleBackColor = true;
            this.Button_Close.Click += new System.EventHandler(this.Button_Close_Click);
            // 
            // Form_Server
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1084, 788);
            this.Controls.Add(this.Button_Close);
            this.Controls.Add(this.Button_Begin);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.jogador2);
            this.Controls.Add(this.jogador1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.TextBox_Porto);
            this.Controls.Add(this.TextBox_IP);
            this.Controls.Add(this.Button_Connect);
            this.Controls.Add(this.PictureBox_Board);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_Server";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Server";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Dado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_mc1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_P2)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_P1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jogador2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jogador1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Board)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PictureBox_Board;
        private System.Windows.Forms.Button Button_Connect;
        private System.Windows.Forms.TextBox TextBox_IP;
        private System.Windows.Forms.TextBox TextBox_Porto;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox jogador1;
        private System.Windows.Forms.PictureBox jogador2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox ComboBox_Cor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox PictureBox_P1;
        private System.Windows.Forms.TextBox TextBox_Nome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Button_Begin;
        private System.Windows.Forms.PictureBox PictureBox_Dado;
        private System.Windows.Forms.Label Label_NomeP1;
        private System.Windows.Forms.PictureBox pictureBox_mc1;
        private System.Windows.Forms.PictureBox PictureBox_P2;
        private System.Windows.Forms.Button Button_Dice;
        private System.Windows.Forms.Label Label_DiceNumber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label Label_NomeP2;
        private System.Windows.Forms.Button Button_Close;
    }
}

