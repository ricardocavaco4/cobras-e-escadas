﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serpentes_Escadas_Server_
{
    public class Mapa
    {

        public int X { get; set; }

        public int Y { get; set; }

        public int NmrQuadrado { get; set; }

        public Mapa(int x, int y, int nmr)
        {

            X = x;
            Y = y;
            NmrQuadrado = nmr;

        }


        static public List<Mapa> CriarMapas()
        {

            List<Mapa> mapa = new List<Mapa>
            {

                {new Mapa(16, 46, 100 ) }, {new Mapa(106, 46, 99 ) },{new Mapa(196, 46, 98 ) }, {new Mapa(286, 46, 97 ) },{new Mapa(376, 46, 96 ) }, {new Mapa(466, 46, 95 ) },{new Mapa(556, 46, 94 ) }, {new Mapa(646, 46, 93 ) },{new Mapa(736, 46, 92 ) }, {new Mapa(826, 46, 91 ) },
                {new Mapa(16, 129, 81 ) }, {new Mapa(106, 129, 82 ) },{new Mapa(196, 129, 83 ) }, {new Mapa(286, 129, 84 ) },{new Mapa(376, 129, 85 ) }, {new Mapa(466, 129, 86 ) },{new Mapa(556, 129, 87 ) }, {new Mapa(646, 129, 88 ) },{new Mapa(736, 129, 89 ) }, {new Mapa(826, 129, 90 ) },
                {new Mapa(16, 212, 80 ) }, {new Mapa(106, 212, 79 ) },{new Mapa(196, 212, 78 ) }, {new Mapa(286, 212, 77 ) },{new Mapa(376, 212, 76 ) }, {new Mapa(466, 212, 75 ) },{new Mapa(556, 212, 74 ) }, {new Mapa(646, 212, 73 ) },{new Mapa(736, 212, 72 ) }, {new Mapa(826, 212, 71 ) },
                {new Mapa(16, 295, 61 ) }, {new Mapa(106, 295, 62 ) },{new Mapa(196, 295, 63 ) }, {new Mapa(286, 295, 64 ) },{new Mapa(376, 295, 65 ) }, {new Mapa(466, 295, 66 ) },{new Mapa(556, 295, 67 ) }, {new Mapa(646, 295, 68 ) },{new Mapa(736, 295, 69 ) }, {new Mapa(826, 295, 70 ) },
                {new Mapa(16, 378, 60 ) }, {new Mapa(106, 378, 59 ) },{new Mapa(196, 378, 58 ) }, {new Mapa(286, 378, 57 ) },{new Mapa(376, 378, 56 ) }, {new Mapa(466, 378, 55 ) },{new Mapa(556, 378, 54 ) }, {new Mapa(646, 378, 53 ) },{new Mapa(736, 378, 52 ) }, {new Mapa(826, 378, 51 ) },
                {new Mapa(16, 461, 41 ) }, {new Mapa(106, 461, 42 ) },{new Mapa(196, 461, 43 ) }, {new Mapa(286, 461, 44 ) },{new Mapa(376, 461, 45 ) }, {new Mapa(466, 461, 46 ) },{new Mapa(556, 461, 47 ) }, {new Mapa(646, 461, 48 ) },{new Mapa(736, 461, 49 ) }, {new Mapa(826, 461, 50 ) },
                {new Mapa(16, 544, 40 ) }, {new Mapa(106, 544, 39 ) },{new Mapa(196, 544, 38 ) }, {new Mapa(286, 544, 37 ) },{new Mapa(376, 544, 36 ) }, {new Mapa(466, 544, 35 ) },{new Mapa(556, 544, 34 ) }, {new Mapa(646, 544, 33 ) },{new Mapa(736, 544, 32 ) }, {new Mapa(826, 544, 31 ) },
                {new Mapa(16, 627, 21 ) }, {new Mapa(106, 627, 22 ) },{new Mapa(196, 627, 23 ) }, {new Mapa(286, 627, 24 ) },{new Mapa(376, 627, 25 ) }, {new Mapa(466, 627, 26 ) },{new Mapa(556, 627, 27 ) }, {new Mapa(646, 627, 28 ) },{new Mapa(736, 627, 29 ) }, {new Mapa(826, 627, 30 ) },
                {new Mapa(16, 710, 20 ) }, {new Mapa(106, 710, 19 ) },{new Mapa(196, 710, 18 ) }, {new Mapa(286, 710, 17 ) },{new Mapa(376, 710, 16 ) }, {new Mapa(466, 710, 15 ) },{new Mapa(556, 710, 14 ) }, {new Mapa(646, 710, 13 ) },{new Mapa(736, 710, 12 ) }, {new Mapa(826, 710, 11 ) },
                {new Mapa(16, 793, 1 ) }, {new Mapa(106, 793, 2 ) },{new Mapa(196, 793, 3 ) }, {new Mapa(286, 793, 4 ) },{new Mapa(376, 793, 5 ) }, {new Mapa(466, 793, 6 ) },{new Mapa(556, 793, 7 ) }, {new Mapa(646, 793, 8 ) },{new Mapa(736, 793, 9 ) }, {new Mapa(826, 793, 10 ) },

            };

            return mapa;

        }

    }
}
